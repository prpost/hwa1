
public class ColorSort {

   enum Color {red, green, blue};

   public static void main (String[] param) {
      Color[] testArray = new Color[] {
              Color.blue, Color.blue, Color.red, Color.green, Color.blue, Color.red,
              Color.green
      };
      Color[] testArray1 = new Color[] {
              Color.blue, Color.blue, Color.blue, Color.blue, Color.blue, Color.blue,
              Color.blue, Color.blue, Color.blue, Color.blue, Color.blue, Color.blue
      };
      reorder(testArray);
      String result = "";
      for (Color color: testArray) {
         if (color == Color.red)
            result += "R, ";
         else if (color == Color.green)
            result += "G, ";
         else
            result += "B, ";
      }
      System.out.println(result);
   }

   public static void reorder (Color[] balls) {
      int redCount = 0;
      int bluesCount = 0;
      int lastIndex = balls.length - 1;
      for (int i = 0; i < balls.length - bluesCount; i++) {
         if (balls[i] == Color.blue) {
            int toIndex;

            // Käime tagant poolt massiivi läbi kuni jõuame esimese pallini, mis ei ole sinine
            do {
               toIndex = lastIndex - bluesCount; // last place index for not blue balls
               bluesCount++;

               // kui leiame tagant poolt esimese palli, mis pole sinine, katkestame tsükli
            } while (toIndex >= 0 && balls[toIndex] == Color.blue);

            // toIndex on viimase palli indeks, mis ei ole sinine
            // kui oleme tsüklis jõudnud juba kaugemale, kui esimene ainult siniste pallide indeks, siis katkestame for-tsükli
            if (toIndex < i) break;

            Color lastBallColorBeforeBlues = balls[toIndex];
            balls[toIndex] = Color.blue; // paneb sinise palli enne esimest sinist palli (massiivi lõpp on korrastatud ja seal vaid sinised pallid)

            // Kui sinine pall oli vaja tõsta enne siniseid järjestatud palle, tuleb selle koha peal olnud pall oma õigesse kohta tõsta
            if (lastBallColorBeforeBlues == Color.red) {
               lastBallColorBeforeBlues = balls[redCount];
               balls[redCount] = Color.red; // put after the ordered reds, at balls[reds]

               // Kontrollime, et ega punane pall ei ole juba oma õigel kohal
               if (i != redCount)
                  balls[i] = lastBallColorBeforeBlues;
               redCount++;
            }
            else { // green
               balls[i] = lastBallColorBeforeBlues; // Kui viimane pall enne siniseid, on roheline, siis jätame ta paika
            }
         }
         else if (balls[i] == Color.red) {
            Color temp = balls[redCount];
            balls[redCount] = Color.red;
            if (i != redCount)
               balls[i] = temp;
            redCount++;
         }
      }
   }
}

